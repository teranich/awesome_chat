Ext.define('awesome_chat.view.main.Main', {
    extend: 'Ext.container.Container',
    requires: [
        'awesome_chat.view.main.MainController',
        'awesome_chat.view.main.MainModel'
    ],

    xtype: 'app-main',
    listeners: {
        'resize': function () {
            this.lookupReference('chatWindow').center()
        }
    },
    controller: 'main',
    viewModel: {
        type: 'main'
    },

    layout: {
        type: 'border'
    },

    items: [{
        xtype: 'window',
        bind: {
            title: '{name}'
        },
        reference: 'chatWindow',
        region: 'center',
        width: 300,
        height: 450,
        autoShow: true,
        layout: 'fit',
  
        items: [{
            xtype: 'dataview',
            reference: 'postView',
            autoScroll: true,
            overItemCls: 'x-item-over',
            emptyText: 'No posts to display',
            tpl: new Ext.XTemplate(
            '<tpl for=".">',
                '<div class="post-item-wrapper">',
                    '<div class="awesome-chat-chater">',
                        '<img src="resources/{userId}.png" alt="" class="awesome-chat-photo">',
                    '</div>',
                    '<div class="awesome-chat-text">',
                        '<div class="awesome-chat-date">{[this.dateFormat(values.time)]}</div>',
                        '<p class="awesome-chat-message">{text}</p>',
                    '</div>',
                '</div>',
            '</tpl>', {
                dateFormat: function (timestamp) {
                    return Ext.Date.format(new Date(timestamp), 'd-m-Y\\ H:i')
                }
            }
           
            ),
            itemSelector: 'div.post-item-wrapper',
            bind: {
                store: '{postStore}'
            },
            listeners: {
                select: 'onPostSelect'
            }
        }],
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            layout: 'fit',
            items: [{
                xtype: 'textfield',
                bind: {
                    value: '{filterString}'
                }
            }]

        },{
            xtype: 'toolbar',
            dock: 'bottom',
            layout: 'fit',
            items: [{
                xtype: 'textareafield',
                reference: 'postTextarea',
                enterIsSpecial: true,
                listeners: {
                    specialkey: 'onSendPost'   
                }
            }]
        }]
    }]
});
