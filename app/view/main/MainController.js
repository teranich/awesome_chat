Ext.define('awesome_chat.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.window.MessageBox'
    ],
    alias: 'controller.main',
    onSendPost: function (comp, e) {
        if (e.getKey() === e.ENTER) {
            var dataview = this.lookupReference('postView'),
                store = dataview.getStore(),
                timestamp = new Date().getTime();

            store.add({
                userId: store.count() % 2 === 0 ? '0': '1',
                text: Ext.util.Format.stripTags(comp.getValue()),
                time: timestamp
            });
            store.sync();
            store.reload();
            comp.reset();
            dataview.scrollTo(0, -1);
            e.preventDefault();
        }
    },
    onPostSelect: function (dataview, record) {
        Ext.Msg.show({
            title:'Actions',
            message: 'Delete the Post?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    var store = dataview.getStore();
                    store.remove(record);
                    store.sync();
                }
            }
        });
    }
});
