Ext.define('awesome_chat.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.main',
    requires: [

    ],
    stores: {
	    postStore:{
	        	autoLoad: true,
	        	fields: [
					{ name: 'time', type: 'int' },
					{ name: 'userId', type: 'string' },   
					{ name: 'text', type: 'string' }
	            ],
	             proxy: {
			        type: 'localstorage',
			        id: 'Posts'
			    }
	    }
	},	
    data: {
        name: 'awesome_chat',
        filterString: '',
    },
    formulas: {
        filterString: {
            get: function (get) {
            	return ''
            },
            set: function (value) {
               var store = this.storeInfo.postStore;
               store.filter('text', value);
            }
        }
    }
});
